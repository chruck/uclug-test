#!/usr/bin/ruby

# From "Demo: CI/CD with GitLab" by Joshua Lambert on GitLab channel:
# https://www.youtube.com/watch?v=1iXFbchozdY

require 'webrick'

server = WEBrick::HTTPServer.new :Port => 5000

# The following proc is used to customize the server operations
server.mount_proc '/' do |request, response|
        response.body = 'Hello, world!'
end

server.start
