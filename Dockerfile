# From "Demo: CI/CD with GitLab" by Joshua Lambert on GitLab channel:
# https://www.youtube.com/watch?v=1iXFbchozdY

FROM ruby:2.4.0-alpine
ADD ./ /app
WORKDIR /app
ENV PORT 5000
EXPOSE 5000

CMD ["sh", "-c", "while :; do ruby ./server.rb; done"]
